/**
* @file	    cifra.cpp
* @brief	Arquivo com a função principal do programa, que cifra as mensagens 
            de acordo com a cifra de vigenère
* @author   Pedro Emerick (p.emerick@live.com)
* @since	08/08/2018
* @date	    13/08/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <cstdlib>
using std::atoi;

#include <string>
using std::string;

#include <cstring>

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include <algorithm>
using std::find;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "--> Erro ao ler mensagem !" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
        mensagem += "\n";
    }

    arquivo.close();

    return mensagem;
}

/**
* @brief Função que grava a mensagem cifrada em um arquivo
* @param mensagem Mensagem cifrada a ser gravada no arquivo
* @param nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq) 
{
    ofstream arquivo (nome_arq);

    if (!arquivo)
    {
        cerr << "--> Erro ao gravar mensagem cifrada !" << endl;
        exit (1);
    }

    arquivo << mensagem << endl;
    arquivo.close ();
}

/**
* @brief Função que cifra uma mensagem de acordo com a chave, através da cifra de vigenère
* @param mensagem Mensagem a ser cifrada
* @param senha Chave/senha para cifrar a mensagem
* @return Mensagem cifrada
*/
string cifrar (string mensagem, string senha) 
{
    string msg_cifrada;
    vector <char> alfabeto;
    for (int ii = 32; ii <= 127; ii++) {
        alfabeto.push_back(ii);
    }
    
    for (int ii = 0, jj = 0; ii < (int) mensagem.length(); ii++) {

        if (mensagem[ii] >= 32 && mensagem[ii] <= 127) {
            vector<char>::iterator it_msg = find(alfabeto.begin(), alfabeto.end(), mensagem[ii]);
            vector<char>::iterator it_key = find(alfabeto.begin(), alfabeto.end(), senha[jj]);

            int indice_msg = (it_msg - alfabeto.begin());
            int indice_key = (it_key - alfabeto.begin());

            int indice_char = (indice_msg + indice_key) % 95;

            char caracter = alfabeto[indice_char];
            
            msg_cifrada += caracter;
            jj = (jj + 1) % (int) senha.length();
        } else {
            msg_cifrada += mensagem[ii];
        }
    }

    return msg_cifrada;
}

/**
* @brief Função principal do programa.
*/
int main (int argc, char *argv[])
{
    // Verifica se são passados os argumentos corretamente
    if (argc != 4) {
        cout << "--> Argumentos invalidos ! Use './prog chave texto_claro texto_cifrado'" << endl;
        exit(1);
    }

    string senha = argv[1];

    string mensagem = ler_msg(argv[2]);

    string msg_cifrada = cifrar (mensagem, senha);

    gravar_msg (msg_cifrada, argv[3]);

    cout << "--> Mensagem cifrada com sucesso !" << endl;

    return 0;
}