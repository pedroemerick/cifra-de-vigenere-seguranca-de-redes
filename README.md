## CIFRA DE VIGENÈRE

Este repositório contém dois programas um que permitem cifrar e um decifrar um arquivo texto seguindo a [Cifra de Vigenère](https://pt.wikipedia.org/wiki/Cifra_de_Vigen%C3%A8re).

---

### Considerações Gerais:

Os programas que permitem cifrar e decifrar utilizam o alfabeto da tabela ASCII do valor 32 ao 127, quaisquer outros caracteres que não sejam deste alfabeto usado, serão ignorados, tanto na cifragem quanto na decifragem.

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila os dois programas;
* "make cifra" = compila o programa que permite cifrar;
* "make decifra" = compila o programa que permite decifrar;
* "make doc" = gera a documentação dos programas, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para cifrar:
* Compile o programa que permite cifrar, usando o comando "make cifra";
* Execute passando os parâmetros corretos:  "./bin/cifra chave mensagem.txt mensagem.sec".
    * chave = a chave escolhida para cifragem
    * mensagem.txt = arquivo que contém a mensagem a ser cifrada
    * mensagem.sec = arquivo em que a mensagem cifrada deve ser armazenada

---

### Para decifrar (com força bruta):
* Compile o programa que permite decifrar, usando o comando "make decifra";
* Execute passando os parâmetros corretos:  "./bin/decifra chave mensagem.sec";
    * chave = a chave escolhida para decifragem
    * mensagem.sec = arquivo que contém a mensagem cifrada
* A mensagem decifrada será impressa na saída padrão.


