LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

RM = rm -rf
RM_TUDO = rm -fr

PROG = cifra
PROG2 = decifra

.PHONY: all clean debug doc doxygen gnuplot init valgrind

all: init $(PROG) $(PROG2)

debug: CFLAGS += -g -O0
debug: $(PROG)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

################## CIFRA ###################
$(PROG): $(OBJ_DIR)/cifra.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/cifra.o: $(SRC_DIR)/cifra.cpp
	$(CC) -c $(CPPFLAGS) -o $@ $<

################## DECIFRA ###################
$(PROG2): $(OBJ_DIR)/decifra.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG2) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/decifra.o: $(SRC_DIR)/decifra.cpp
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes -v ./bin/multimat 2 4 8 16 32

doxygen:
	doxygen -g

doc:
	@mkdir -p $(DOC_DIR)/
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
